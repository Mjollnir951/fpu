LIBRARY ieee, std;
USE ieee.std_logic_1164.ALL;
USE std.textio.ALL;
USE ieee.std_logic_textio.ALL;

ENTITY symulacja IS
END symulacja;
 
ARCHITECTURE behavior OF symulacja IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT sumator
    PORT(
         N1 : IN  std_logic_vector(31 downto 0);
         N2 : IN  std_logic_vector(31 downto 0);
			operacja : IN  std_logic_vector(1 downto 0);
         clk : IN  std_logic;
         rst : IN  std_logic;
         wynik : OUT  std_logic_vector(31 downto 0);
			symulacja : OUT std_logic
        );
    END COMPONENT;
    

   --Inputs
   signal N1 : std_logic_vector(31 downto 0) := (others => '0');
   signal N2 : std_logic_vector(31 downto 0) := (others => '0');
	signal operacja : std_logic_vector(1 downto 0) := (others => '0');
   signal clk : std_logic := '0';
   signal rst : std_logic := '0';
	signal strobe : std_logic;

 	--Outputs
   signal wynik : std_logic_vector(31 downto 0);
	signal symulacja : std_logic;

   -- Clock period definitions
   constant clk_period : time := 10 ns;
	constant p10 : time := clk_period/10;
	constant edge : time := clk_period-p10;
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: sumator PORT MAP (
          N1 => N1,
          N2 => N2,
			 operacja => operacja,
          clk => clk,
          rst => rst,
          wynik => wynik,
			 symulacja => symulacja
        );

   -- Clock process definitions
   clk_process :process
   begin
		clk <= '0';
		wait for clk_period/2;
		clk <= '1';
		wait for clk_period/2;
   end process;
 
	-------- op�nienie sygna�u strobuj�cego ---------
	strobe <= TRANSPORT clk AFTER edge;

	-------- zapis danych do pliku - proces: �output" -------- 
	output: PROCESS (strobe)
	variable str        :string(1 to 98);
	variable lineout    :line;
	variable init_file  :std_logic := '1';
	file outfile        :text is out "wyniki.txt";
	-------- funkcja konwersji: std_logic => character -------- 
	FUNCTION conv_to_char (sig: std_logic) RETURN character IS
	BEGIN
		CASE sig IS
			WHEN '1'     => return '1';
			WHEN '0'     => return '0';
			WHEN 'Z'     => return 'Z';
			WHEN others  => return 'X';
		END CASE;
	END conv_to_char;
	-------- funkcja konwersji: std_logic_vector => string -------- 
	FUNCTION conv_to_string (inp: std_logic_vector; length: integer) RETURN string IS
	VARIABLE s : string(1 TO length);
	BEGIN
		FOR i IN 0 TO (length-1) LOOP
			s(length-i) := conv_to_char(inp(i));
		END LOOP;
		RETURN s;
	END conv_to_string;
	-------------------------------------
	BEGIN
	-------- nag��wek pliku wyj�ciowego (podzia� kolumn) -------- 
		IF init_file = '1' THEN
			str:="               N1               |               N2               |             wynik              ";
				write(lineout,str); writeline(outfile,lineout);
			init_file := '0';
		END IF;
		-------- zapis danych do pliku wyjsciowego �wyjscie" -------- 
		--IF (strobe'EVENT AND strobe='0') THEN --gdy chcemy rowniez wyswietlac 0 na N1,N2 i wyniku w momencie gdy rst=1
		IF (strobe'EVENT AND strobe='0' AND rst='0') THEN --gdy chcemy wyswietlac tylko N1,N2 i wynik w momencie gdy rst=0
			str    := (others => ' ');
			str(1 to 32) := conv_to_string(N1,32);
			str(33)  := ' ';
			str(34 to 65) := conv_to_string(N2,32);
			str(66)  := ' ';
			str(67 to 98) := conv_to_string(wynik,32);
			write(lineout,str);
			writeline(outfile,lineout);
			str    := (others => ' ');
			write(lineout,str);
			writeline(outfile,lineout);
		END IF;
	END PROCESS output;

   -- Stimulus process
   stim_proc: process
   begin
		--tu zaczynamy przypisania
		rst <= '1'; 
		wait for 10 ns;
		rst <= '0';
		
--		--takie same znaki
--		N1 <= "01000000000000000000000000000000";--2
--		N2 <= "01000000010000000000000000000000";--3
--		wait for 10 ns;
--		rst <= '1';
--		wait for 10 ns;
--		rst <= '0';
--		
--		N1 <= "01000000000011001100110011001101";--2.2
--		N2 <= "01000000010000000000000000000000";--3

--		N1 <= "01000000000000000000000000000000";--2
--		N2 <= "01000000010100110011001100110011";--3.3
		
--		N1 <= "01000000000011001100110011001101";--2.2
--		N2 <= "01000000010100110011001100110011";--3.3
		
--		N1 <= "01000000001000000000000000000000";--2.5
--		N2 <= "01000001100001000000000000000000";--16.5

--		N1 <= "01000000001000000000000000000000";--2.5
--		N2 <= "01000001111111000000000000000000";--31.5, zeby bylo przeniesienie ci(23)=1
		
--		N1 <= "00111111011000111101011100001010";--0.89
--		N2 <= "01000010110010000000010100011111";--100.01
		
		--rozne znaki
--		N1 <= "01000000010000000000000000000000";--(3)
--		N2 <= "11000000000000000000000000000000";--(-2)
--		BLAD, bo przy xorowaniu szuka 1 i nie moze jej znalezc
		
--		N1 <= "11000000000011001100110011001101";--(-2.2)
--		N2 <= "01000000010100110011001100110011";--(3.3)
		
--		N1 <= "11000000001000000000000000000000";--(-2.5)
--		N2 <= "01000000100100000000000000000000";--(4.5)
		
		
		
		
		
		
		
--		N1 <= "11000000001000000000000000000000";--(-2.5)
--		N2 <= "01000001000010000000000000000000";--(8.5)

--		N1 <= "10111111011000111101011100001010";--(-0.89)
--		N2 <= "01000010110010000000010100011111";--100.01


		wait;
   end process;
END;