--------------------------------------------------------------------------------
-- Company: 
-- Engineer:
--
-- Create Date:   13:01:45 04/30/2016
-- Design Name:   
-- Module Name:   C:/Users/Adam/Documents/Xilinx_projects/FPU/symulacja.vhd
-- Project Name:  FPU
-- Target Device:  
-- Tool versions:  
-- Description:   
-- 
-- VHDL Test Bench Created by ISE for module: sumator
-- 
-- Dependencies:
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
-- Notes: 
-- This testbench has been automatically generated using types std_logic and
-- std_logic_vector for the ports of the unit under test.  Xilinx recommends
-- that these types always be used for the top-level I/O of a design in order
-- to guarantee that the testbench will bind correctly to the post-implementation 
-- simulation model.
--------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--USE ieee.numeric_std.ALL;
 
ENTITY symulacja IS
END symulacja;
 
ARCHITECTURE behavior OF symulacja IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT sumator
    PORT(
         N1 : IN  std_logic_vector(31 downto 0);
         N2 : IN  std_logic_vector(31 downto 0);
         operacja : IN  std_logic_vector(1 downto 0);
         clk : IN  std_logic;
         rst : IN  std_logic;
         wynik : OUT  std_logic_vector(31 downto 0);
         symulacja : OUT  std_logic
        );
    END COMPONENT;
    

   --Inputs
   signal N1 : std_logic_vector(31 downto 0) := (others => '0');
   signal N2 : std_logic_vector(31 downto 0) := (others => '0');
   signal operacja : std_logic_vector(1 downto 0) := (others => '0');
   signal clk : std_logic := '0';
   signal rst : std_logic := '0';

 	--Outputs
   signal wynik : std_logic_vector(31 downto 0);
   signal symulacja : std_logic;

   -- Clock period definitions
   constant clk_period : time := 10 ns;
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: sumator PORT MAP (
          N1 => N1,
          N2 => N2,
          operacja => operacja,
          clk => clk,
          rst => rst,
          wynik => wynik,
          symulacja => symulacja
        );

   -- Clock process definitions
   clk_process :process
   begin
		clk <= '0';
		wait for clk_period/2;
		clk <= '1';
		wait for clk_period/2;
   end process;
 

   -- Stimulus process
   stim_proc: process
   begin		
      -- hold reset state for 100 ns.
      wait for 100 ns;	

      wait for clk_period*10;

      -- insert stimulus here 

      wait;
   end process;

END;
