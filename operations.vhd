--Pomysly:
--dodac wyjatki (dodawanie liczb gdy E=0, gdy obie -0 itp.)
--dodac zaokraglenia, ale to na koncu

--przypisanie wartosci dac przy deklaracji zmiennych

--Info:
--wyjscie "symulacja" zostalo dodane po to, zeby w przypadku wypisywania wynikow symulacji do pliku 
--wynik byl wypisywany tylko wtedy gdy zostanie obliczony, ale chyba jest to zbedne (dziala dobrze bez tego)
--przy konczeniu projektu mozna to usunac

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;--do zamiany integer na vector

entity sumator is
    Port ( N1 : in  STD_LOGIC_VECTOR (31 downto 0);
           N2 : in  STD_LOGIC_VECTOR (31 downto 0);
			  operacja : in STD_LOGIC_VECTOR (1 downto 0);
			  clk : in STD_LOGIC;
			  rst : in STD_LOGIC;
           wynik : out  STD_LOGIC_VECTOR (31 downto 0);
			  symulacja : out STD_LOGIC
			  );
end sumator;

architecture Behavioral of sumator is

begin

process (clk)

variable liczba1 : STD_LOGIC_VECTOR(31 downto 0); --liczba 1
variable S1 : STD_LOGIC; --znak liczby 1
variable E1 : STD_LOGIC_VECTOR(7 downto 0); --wykladnik liczby 1
variable M1 : STD_LOGIC_VECTOR(22 downto 0); --mantysa liczby 1
variable U1 : STD_LOGIC; --ukryty znak liczby 1 (zerowany gdy E1=0)

variable liczba2 : STD_LOGIC_VECTOR(31 downto 0); --liczba 2
variable S2 : STD_LOGIC; --znak liczby 2
variable E2 : STD_LOGIC_VECTOR(7 downto 0); --wykladnik liczby 2
variable M2 : STD_LOGIC_VECTOR(22 downto 0); --mantysa liczby 2
variable U2 : STD_LOGIC; --ukryty znak liczby 2 (zerowany gdy E2=0)

variable tmp : STD_LOGIC_VECTOR(31 downto 0) := "00000000000000000000000000000000"; --zmienna pomocnicza do zamiany liczb N1 i N2
--variable tmp10 : integer := 0;
--variable tmp11 : integer := 0;
--variable tmp12 : integer := 0;
--variable tmp13 : integer := 0;
--variable tmp14 : integer := 0;
--variable tmp15 : integer := 0;
--variable tmp16 : integer := 0;
--variable tmp17 : integer := 0;
--variable tmp20 : integer := 0;
--variable tmp21 : integer := 0;
--variable tmp22 : integer := 0;
--variable tmp23 : integer := 0;
--variable tmp24 : integer := 0;
--variable tmp25 : integer := 0;
--variable tmp26 : integer := 0;
--variable tmp27 : integer := 0;
variable E1d : integer; --wykladnik liczby 1 dziesietnie
variable E2d : integer; --wykladnik liczby 2 dziesietnie
variable d : integer; --roznica E1-E2 dziesietnie
variable M2tmp : STD_LOGIC_VECTOR(22 downto 0); --tmp do przesuwania przecinka w M2 (do normalizacji, step 4)
variable suma : STD_LOGIC_VECTOR(23 downto 0); --wynik S1+S2 (step 6), kazdy bit odpowiada za bit w sumatorze 24 bit
variable ci : STD_LOGIC_VECTOR(23 downto 0); --c_in, do powyzszego sumatora, 23 dlatego, bo ostatnie ci jest dla dodawania S1+S2
variable co : STD_LOGIC_VECTOR(23 downto 0);--c_out, do powyzszego sumatora, 23 dlatego, bo ostatnie ci jest dla dodawania S1+S2
variable changed : STD_LOGIC := '0'; --zmienna do poinformowania, ze zamiana liczb (N1=N2, N2=N1) zostala dokonana
variable z : integer; --zmienna wykorzystywana przy step 7 gdy sa rozne znaki, przy xorowaniu bitow - dopelnienie
variable dzialanie : STD_LOGIC_VECTOR(1 downto 0); --zmienna okreslajaca dzialanie, ktore wykonujemy (00-dodawanie, 01-mnozenie)

begin

liczba1(31 downto 0) := N1(31 downto 0);
liczba2(31 downto 0) := N2(31 downto 0);
S1 := liczba1(31);
S2 := liczba2(31);
E1 := liczba1(30 downto 23);
E2 := liczba2(30 downto 23);
M1 := liczba1(22 downto 0);
M2 := liczba2(22 downto 0);
U1 := '1';
U2 := '1';
dzialanie(1 downto 0) := operacja(1 downto 0);

tmp := "00000000000000000000000000000000";
E1d := 0;
E2d := 0;
d := 0;
M2tmp := "00000000000000000000000";
suma := "000000000000000000000000";
ci := "000000000000000000000000";
co := "000000000000000000000000";
changed := '0';
z := 0;


symulacja <= '0';

if rising_edge(clk) then
	if(rst = '1') then
		wynik <= "00000000000000000000000000000000";
	else
		if(dzialanie = "00") then --dodawanie
			--Przypadek pierwszy - liczby maja takie same znaki
			if(S1 = S2) then --jesli takie same znaki liczb
				--step 2:
				--zeruje bit ukryty gdy mantysa = 0
				if(E1 = "0") then
					U1 := '0';
				elsif(E2 = "0") then
					U2 := '0';
				end if;
				
				--gdy mantysa rozna od 0
				--sprawdzam czy E2 > od E1, jesli tak to zamieniam liczby. teraz N1 = N2 a N2 = N1. E1 musi byc wieksze od E2
				if(E2(7) > E1(7)) then
					tmp := liczba1;
					liczba1 := liczba2;
					liczba2 := tmp;
					changed := '1';
				elsif((E2(6) > E1(6)) and (changed = '0')) then
					tmp := liczba1;
					liczba1 := liczba2;
					liczba2 := tmp;
					changed := '1';
				elsif((E2(5) > E1(5)) and (changed = '0')) then
					tmp := liczba1;
					liczba1 := liczba2;
					liczba2 := tmp;
					changed := '1';
				elsif((E2(4) > E1(4)) and (changed = '0')) then
					tmp := liczba1;
					liczba1 := liczba2;
					liczba2 := tmp;
					changed := '1';
				elsif((E2(3) > E1(3)) and (changed = '0')) then
					tmp := liczba1;
					liczba1 := liczba2;
					liczba2 := tmp;
					changed := '1';
				elsif((E2(2) > E1(2)) and (changed = '0')) then
					tmp := liczba1;
					liczba1 := liczba2;
					liczba2 := tmp;
					changed := '1';
				elsif((E2(1) > E1(1)) and (changed = '0')) then
					tmp := liczba1;
					liczba1 := liczba2;
					liczba2 := tmp;
					changed := '1';
				elsif((E2(0) > E1(0)) and (changed = '0')) then
					tmp := liczba1;
					liczba1 := liczba2;
					liczba2 := tmp;
					changed := '1';
				end if;
			
				--step 3:
				--ustawiam S, R, M po zamianie liczb
				S1 := liczba1(31);
				E1 := liczba1(30 downto 23);
				M1 := liczba1(22 downto 0);
				
				S2 := liczba2(31);
				E2 := liczba2(30 downto 23);
				M2 := liczba2(22 downto 0);
						
				--licze d=E1-E2
				--zamieniam E1 na dziesietny
	--			if(E1(7) = '1') then
	--				tmp17 := tmp17 + 128;
	--			elsif(E1(6) = '1') then
	--				tmp17 := tmp17 + 64;
	--			elsif(E1(5) = '1') then
	--				tmp17 := tmp17 + 32;
	--			elsif(E1(4) = '1') then
	--				tmp17 := tmp17 + 16;
	--			elsif(E1(3) = '1') then
	--				tmp17 := tmp17 + 8;
	--			elsif(E1(2) = '1') then
	--				tmp17 := tmp17 + 4;
	--			elsif(E1(1) = '1') then
	--				tmp17 := tmp17 + 2;
	--			elsif(E1(0) = '1') then
	--				tmp17 := tmp17 + 1;
	--			end if;
	--			E1d := tmp17;
				
				E1d := to_integer(unsigned(E1));
				
				--zamieniam E2 na dziesietny
	--			if(E2(7) = '1') then
	--				tmp27 := 128;
	--			elsif(E2(6) = '1') then
	--				tmp26 := 64;
	--			elsif(E2(5) = '1') then
	--				tmp25 := 32;
	--			elsif(E2(4) = '1') then
	--				tmp24 := 16;
	--			elsif(E2(3) = '1') then
	--				tmp23 := 8;
	--			elsif(E2(2) = '1') then
	--				tmp22 := 4;
	--			elsif(E2(1) = '1') then
	--				tmp21 := 2;
	--			elsif(E2(0) = '1') then
	--				tmp20 := 1;
	--			end if;
	--			E2d := tmp27+tmp26+tmp25+tmp24+tmp23+tmp22+tmp21+tmp20;
				
				E2d := to_integer(unsigned(E2));
				
				--licze roznice mantys
				d := E1d-E2d;
				--if(d = '0') then gdy d=0 to nic nie robie (nie musze przesuwac mantysy
				--end if;
				
				if(d > 0) then --gdy d>0 (czyli E1>E2) to przesuwam S2 w prawo o ilosc = d (zeby wyrownac mantysy E1 i E2)
					M2tmp := M2; --kopia
					
					if(d >= 2) then --ustawiam zera na poczatku
						--bylo--for i in 23 to 25-d loop
						for i in 22 downto 24-d loop
							M2(i) := '0';
						end loop;
					end if;
					--gdy d < 2 to nie musze dawac zer na poczatku
					
					
					--bylo--M2(24-d) := '1'; --ustawiam 1 (to ta jedynka, ktora byla przed przecinkiem)
					M2(23-d) := '1'; --ustawiam 1 (to ta jedynka, ktora byla przed przecinkiem)
					--TUTAJ SIE DZIEJE MAGIA
					--bylo--for i in 0 to 23-d loop --przesuwam reszte bitow
					for i in 0 to 22-d loop --przesuwam reszte bitow
						M2(i) := M2tmp(i+d);
					end loop;
					U2 := '0';
				end if;

				--step 4:
				E2d := E2d + d; --zwiekszam wykladnik liczby 2 o taka ilosc o jaka przesuwalem przecinek
				--teraz E1 = E2, czyli mamy liczby znormalizowane

				--step 5:
				--liczba 1 i liczba 2 maja takie same znaki

				--step 6:
				--trzeba mi sumator 24 bitowy z przeniesieniem
				ci(0) := '0';
				for i in 0 to 22 loop
					suma(i) := M1(i) xor M2(i) xor ci(i);
					co(i) := (M1(i) and M2(i)) or (M1(i) and ci(i));
					--if (i < 23) then --bez tego, bo jeszcze S1+S2
					ci(i+1) := co(i);
					--end if;
				end loop;
				
				--hidden bit:
				suma(23) := U1 xor U2 xor ci(23);
				co(23) := (U1 and U2) or (U1 and ci(23));

				--step 7: --jesli przeniesienie ci(23) = 1 to znaczy, ze na 23 bicie dodajemy:
				--jedna jedynke (bo drugi ukryty bit ustawilismy na 0 po przesuwaniu bitow mantysy) + przeniesienie na 
				--ta pozycje = 1, to spowodowuje przeniesienie na pozycje 24 (ci(24)=1 <=> co(23)=1), co zachodzi tylko 
				--w przypadku gdy ci(23)=1
				--co musze teraz zrobic:
				--dodaje 1 do E (obojetnego ktorego, ja dodaje do E2), aby moc przesunac przecinek przecinek w "sumie" w lewo 
				--(czyli bity w prawo). po przesunieciu bit ukryty znowu bedzie rowny 1 (czyli mamy jedynke przed przecinkiem)
				if (co(23) = '1') then    	-- je�li E = max to zdenormalizowana
					E2d := E2d + 1;
					for i in 0 to 22 loop		-- przesuni�cie bit�w o 1 w prawo
						suma(i) := suma (i+1);
					end loop;
					suma(23) := '1'; 		-- w tym przypadku carry bit zawsze = 1, czyli nasza ukryta 1. ten bit suma(23) bedzie 
					--uzywany do liczb zdenormalizowanych, ale to potem
					--TUTAJ chyba trzeba zwiekszyc o 1 E1d i przesunac bity w M1
					--chyba jednak NIE, bo to jest juz wynik, wiec nie trzeba tego robic
				end if;

				--step 8:
				
				--step 9:
				wynik(31) <= S1; --S2 tez moze byc, bo maja takie same znaki
				
				--step 10:
				E2 := std_logic_vector(to_unsigned(E2d,8));
				wynik(30 downto 23) <= E2;
				wynik(22 downto 0) <= suma(22 downto 0);
			
			
			
			
			
			
			

			--Przypadek drugi - liczby maja rozne znaki
			--kroki 2-5 sa takie same
			elsif(S1 /= S2) then --jesli liczby maja rozne znaki to:
				--step 2:
				--zeruje bit ukryty gdy mantysa = 0
				if(E1 = "0") then
					U1 := '0';
				elsif(E2 = "0") then
					U2 := '0';
				end if;
				
				--gdy mantysa rozna od 0
				--sprawdzam czy E2 > od E1, jesli tak to zamieniam liczby. teraz N1 = N2 a N2 = N1. E1 musi byc wieksze od E2
				if(E2(7) > E1(7)) then
					tmp := liczba1;
					liczba1 := liczba2;
					liczba2 := tmp;
					changed := '1';
				elsif((E2(6) > E1(6)) and (changed = '0')) then
					tmp := liczba1;
					liczba1 := liczba2;
					liczba2 := tmp;
					changed := '1';
				elsif((E2(5) > E1(5)) and (changed = '0')) then
					tmp := liczba1;
					liczba1 := liczba2;
					liczba2 := tmp;
					changed := '1';
				elsif((E2(4) > E1(4)) and (changed = '0')) then
					tmp := liczba1;
					liczba1 := liczba2;
					liczba2 := tmp;
					changed := '1';
				elsif((E2(3) > E1(3)) and (changed = '0')) then
					tmp := liczba1;
					liczba1 := liczba2;
					liczba2 := tmp;
					changed := '1';
				elsif((E2(2) > E1(2)) and (changed = '0')) then
					tmp := liczba1;
					liczba1 := liczba2;
					liczba2 := tmp;
					changed := '1';
				elsif((E2(1) > E1(1)) and (changed = '0')) then
					tmp := liczba1;
					liczba1 := liczba2;
					liczba2 := tmp;
					changed := '1';
				elsif((E2(0) > E1(0)) and (changed = '0')) then
					tmp := liczba1;
					liczba1 := liczba2;
					liczba2 := tmp;
					changed := '1';
				end if;
			
				--step 3:
				--ustawiam S, R, M po zamianie liczb
				S1 := liczba1(31);
				E1 := liczba1(30 downto 23);
				M1 := liczba1(22 downto 0);
				
				S2 := liczba2(31);
				E2 := liczba2(30 downto 23);
				M2 := liczba2(22 downto 0);
						
				--licze d=E1-E2
				--zamieniam E1 na dziesietny
	--			if(E1(7) = '1') then
	--				tmp17 := tmp17 + 128;
	--			elsif(E1(6) = '1') then
	--				tmp17 := tmp17 + 64;
	--			elsif(E1(5) = '1') then
	--				tmp17 := tmp17 + 32;
	--			elsif(E1(4) = '1') then
	--				tmp17 := tmp17 + 16;
	--			elsif(E1(3) = '1') then
	--				tmp17 := tmp17 + 8;
	--			elsif(E1(2) = '1') then
	--				tmp17 := tmp17 + 4;
	--			elsif(E1(1) = '1') then
	--				tmp17 := tmp17 + 2;
	--			elsif(E1(0) = '1') then
	--				tmp17 := tmp17 + 1;
	--			end if;
	--			E1d := tmp17;
				
				E1d := to_integer(unsigned(E1));
				
				--zamieniam E2 na dziesietny
	--			if(E2(7) = '1') then
	--				tmp27 := 128;
	--			elsif(E2(6) = '1') then
	--				tmp26 := 64;
	--			elsif(E2(5) = '1') then
	--				tmp25 := 32;
	--			elsif(E2(4) = '1') then
	--				tmp24 := 16;
	--			elsif(E2(3) = '1') then
	--				tmp23 := 8;
	--			elsif(E2(2) = '1') then
	--				tmp22 := 4;
	--			elsif(E2(1) = '1') then
	--				tmp21 := 2;
	--			elsif(E2(0) = '1') then
	--				tmp20 := 1;
	--			end if;
	--			E2d := tmp27+tmp26+tmp25+tmp24+tmp23+tmp22+tmp21+tmp20;
				
				E2d := to_integer(unsigned(E2));
				
				--licze roznice mantys
				d := E1d-E2d;
				--if(d = '0') then gdy d=0 to nic nie robie (nie musze przesuwac mantysy
				--end if;
				
				if(d > 0) then --gdy d>0 (czyli E1>E2) to przesuwam S2 w prawo o ilosc = d (zeby wyrownac mantysy E1 i E2)
					M2tmp := M2; --kopia
					
					if(d >= 2) then --ustawiam zera na poczatku
						--bylo--for i in 23 to 25-d loop
						for i in 22 downto 24-d loop
							M2(i) := '0';
						end loop;
					end if;
					--gdy d < 2 to nie musze dawac zer na poczatku
					
					
					--bylo--M2(24-d) := '1'; --ustawiam 1 (to ta jedynka, ktora byla przed przecinkiem)
					M2(23-d) := '1'; --ustawiam 1 (to ta jedynka, ktora byla przed przecinkiem)
					
					--bylo--for i in 0 to 23-d loop --przesuwam reszte bitow
					for i in 0 to 22-d loop --przesuwam reszte bitow
						M2(i) := M2tmp(i+d);
					end loop;
					U2 := '0';
				end if;

				--step 4:
				E2d := E2d + d; --zwiekszam wykladnik liczby 2 o taka ilosc o jaka przesuwalem przecinek
				--teraz E1 = E2, czyli mamy liczby znormalizowane

				--step 5:
				--liczba 1 i liczba 2 maja rozne znaki
				
				--step 6:
				--Take 2?s complement of S2 - bierzemy mantyse liczby mniejszej i robimy dzialanie:
				--od 0 odejmujemy mantyse co jest rownowazne negacji wszystkich bitow i dodaniu 1
				changed := '0';
				
				--zaczynam od bitu 0 do 22, szukam jedynki, jesli znajde to kolejne bity neguje jesli nie znajde to nic 
				--nie robie z tymi ktore znalazlem
				for i in 0 to 22 loop
					if(changed = '1') then
						M2(i) := not M2(i);
					end if;
					if(M2(i)='1' and changed = '0') then
						changed := '1';
					end if;
				end loop;
				--UWAGA: dodac kod obslugujacy przypadek gdy 1 jest dopiero na ostatnim (22 bicie)

				--dodaje uzupelnienie M2 do M1, wynik w M1
				ci(0) := '0';
				for i in 0 to 22 loop
					suma(i) := M1(i) xor M2(i) xor ci(i);
					co(i) := (M1(i) and M2(i)) or (M1(i) and ci(i));
					ci(i+1) := co(i);
				end loop;
				
				suma(23) := U1 xor U2 xor ci(23);
				co(23) := (U1 and U2) or (U1 and ci(23));
				
				--step 7:
				--jesli mamy carry out, czyli jesli co(22)=ci(23)=1 to mamy pozyczke (niedomiar) underflow, dlatego teraz w sumie
				--musimy przesunac przecinek w prawo (za pierwsza napotkana jedynke) i od wykladnika odejmujemy 
				--wartosc 'z' (rowna ilosci przesuniec przecinka)
				z := 0;
				if(co(23)='1') then
					if(suma(23) = '0') then
						z := z + 1;
						for i in 23 downto 1 loop
							suma(i) := suma(i-1);
							suma(0) := '0';
						end loop;
					end if;
					
					--step 8:
					E2d := E2d - z;
				
				else
					--step 9:
					--jesli natomiast nie mamy carry out czyli zapozyczenia, wtedy mamy pewnosc, ze suma(23)=1, czyli
					--nie ma potrzeby przesuwac przecinka, zeby jedynka byla przed przecinkiem
					--robie uzupelnienie sumy, czyli 0-suma+1 (tak jak wczesniej)
					changed := '0';
					for i in 22 downto 0 loop
						if(changed = '1') then
							suma(i) := not suma(i);
						end if;
						if((suma(i) = '1') and (changed = '0')) then
							changed := '1';
						end if;
					end loop;
				end if;
				
				--step 10:
				--sprawdzam ktora liczba jest wieksza i ustawiam odpowiedni znaku wyniku
				changed := '0';
				for i in 30 downto 0 loop
					if(liczba1(i)>liczba2(i) and changed = '0') then
						wynik(31) <= liczba1(31);
						--exit Petla1;
						changed := '1';
						
					elsif(liczba2(i)>liczba1(i) and changed = '0') then
						wynik(31) <= liczba2(31);
						--exit Petla1;
						changed := '1';
					end if;
				end loop;
				
				E2 := std_logic_vector(to_unsigned(E2d,8));
				wynik(30 downto 23) <= E2;
				wynik(22 downto 0) <= suma(22 downto 0);
		
	--			tmp (31 downto 0):= "01000000000000000000000000000000";
	--			for i in 0 to 30 loop
	--				tmp(i):=tmp(i+1);
	--			end loop;
	--			wynik (31 downto 0) <= tmp(31 downto 0);
		
			end if; --konczenie ifa przy sprawdzaniu czy liczby N1 i N2 maja takie same znaki
			symulacja <= '1';
		
		elsif(dzialanie = "01") then --mnozenie
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		end if; --zakonczenie ifa przy sprawdzaniu jakie dzialanie wykonujemy
	end if; --konczenie ifa przy sprawdzeniu czy rst = 1
end if; --zakonczenie ifa przy rising egde
end process;

end Behavioral;